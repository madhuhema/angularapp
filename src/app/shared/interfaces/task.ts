export interface Task {
    id:string,
    title:string,
    description:string,
    priority:number,
    parentTaskId:string,
    assigneeId:string,
    creatorId:string,
    startDate:Date,
    dueDate:Date,
    createdAt:Date,
    updatedAt:Date
}
