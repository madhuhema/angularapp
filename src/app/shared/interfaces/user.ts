export interface User {
    id:string,
    firstName:string,
    lastName:string,
    email:string,
    supervisorId:string,
    roleId:string
}
