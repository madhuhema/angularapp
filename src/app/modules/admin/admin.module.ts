import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AssignRoleComponent } from './components/assign-role/assign-role.component';
import { MembersComponent } from './components/members/members.component';

@NgModule({
  declarations: [DashboardComponent, AssignRoleComponent, MembersComponent],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
