import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TaskManagerComponent } from './components/task-manager/task-manager.component';

@NgModule({
  declarations: [DashboardComponent, ProfileComponent, TaskManagerComponent],
  imports: [
    CommonModule
  ]
})
export class ManagerModule { }
